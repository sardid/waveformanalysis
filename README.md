# The Waveform Analysis toolbox for cell-type identification (the code has been ported to [GitHub](https://github.com/LofNaDI/waveformAnalysis))

The Waveform Analysis (MATLAB / GNU Octave) toolbox classifies broad vs narrow spiking neurons based on the characteristics of extracellularly recorded action potential (AP) waveforms. This toolbox was developed and is maintained by Salva Ardid PhD.

The code reproduces Figure 1 of the paper: *"Ardid, Vinck, Kaping, Marquez, Everling, Womelsdorf (2015) Mapping of functionally characterized cell classes onto canonical circuit operations in primate prefrontal cortex"*, published in The Journal of Neuroscience.

This toolbox is free but copyrighted by Salva Ardid, and distributed under the terms of the GNU General Public Licence as published by the Free Software Foundation (version 3).

Should you find the toolbox interesting, please try it in your data. Just ensure that any publication using this code properly cites the original manuscript and links to this repository:

> \- Ardid, Vinck, Kaping, Marquez, Everling, Womelsdorf (2015) Mapping of functionally characterized cell classes onto canonical circuit operations in primate prefrontal cortex. [*J. Neurosci.*, 35: 2975-91](http://www.jneurosci.org/content/35/7/2975)
> 
> \- [https://github.com/LofNaDI/waveformAnalysis](https://github.com/LofNaDI/waveformAnalysis)

Copyright (C) 2014-2015, Salva Ardid.
